from embeddings import Embeds
import string
import nltk
import pickle
import random
# import emoji
import re
import enchant
from difflib import SequenceMatcher
from spellchecker import SpellChecker
from collections import Counter
import fileinput
import splitter
import numpy as np
from quantities import units
from gensim.models.keyedvectors import KeyedVectors

unit_symbols = [u.symbol for _, u in units.__dict__.items() if isinstance(u, type(units.deg))]

d = enchant.Dict("en")
spell = SpellChecker()
spell.word_frequency.load_text_file('../clean/training-clean.txt')
spell.distance = 2

np.random.seed(1)


abbr_list = ['lol', 'lmao', 'tbh', 'omg', 'omfg', 'idk', 'wtf', 'idc', 'idgaf']


def write_normed(iv):
    fw.write('{}\tNORMED\t{}\n'.format(iv, iv))


def write_need_norm(oov, iv):
    fw.write("{}\tNEED_NORM\t{}\n".format(oov, iv))


def is_ok(term, w2c_item):
    """ Check word in embeddings if they have similar syntaxis """
    return w2c_item[1] > 0.6 and nltk.edit_distance(term, w2c_item[0]) < 5


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def brown_candidates(word, list):
    brown = {}
    # Load browncluster file and make dictionary of it
    textfile = '../brownclusters/50mpaths.txt'
    with open(textfile) as f:
        for line in f:
            k = line.strip().split('\t')[1]
            v = line.strip().split('\t')[0]
            brown[k.strip()] = v.strip()
    if word in brown:
        for k, v in brown.items():
            if len(k) > 1:
                if brown.get(word) == v and d.check(k) and not k.isdigit() and similar(word, k) >= 0.25 \
                        and not re.findall("[0-9]*[.!?\\-]", k) and (word[0] == k[0] or word[-1] == k[-1]):
                    list.append(k)


# Load Twitter embeddings of MoNoise
embeddings = Embeds()
embeddings.loadBin('en.tw.embeds.bin')

# # Load word2vec Twitter embeddings
model = KeyedVectors.load_word2vec_format("gensim_glove_vectors.txt", binary=False)
print("model loaded")


def distance_candidates(word, cand_list):
    spell_candidates = spell.candidates(word)
    for candidate in spell_candidates:
        if d.check(candidate) and not candidate.isdigit() and not re.findall("[0-9]*[.!?\\-]", candidate)\
                and candidate.lower() not in cand_list:
            cand_list.append(candidate.lower())


def embeddings_candidates(word, cand_list):
    suggestions = embeddings.find(word)
    if suggestions:
        for suggestion in suggestions:
            if len(suggestion) > 1 and d.check(suggestion) and nltk.edit_distance(word, suggestion) < 6 and not suggestion.isdigit()\
                        and not re.findall("[0-9]*[.!?\\-]", suggestion) and suggestion.lower() not in cand_list:
                    cand_list.append(suggestion.lower())


def w2v_embeddings_candidates(word, cand_list):
    try:
        suggestions = model.most_similar(word, topn=50)
        for suggestion in suggestions:
            score = suggestion[1]
            candidate = suggestion[0]
            if score > 0.8 and (d.check(candidate) or d.check(candidate.capitalize())) and candidate.lower() not in cand_list:
                cand_list.append(candidate.lower())
    except KeyError:
        pass


def enchant_candidates(word, cand_list):
    suggestions = d.suggest(word)
    for suggestion in suggestions:
        if len(suggestion) > 1:
            if d.check(suggestion) and not suggestion.isdigit() and not re.findall("[0-9]*[.!?\\-]", suggestion)\
                    and suggestion.lower() not in cand_list:
                cand_list.append(suggestion.lower())


def compound_candidates(word, cand_list):
    suggestions = splitter.split(word)
    compound = []
    if len(suggestions) > 1:
        for suggestion in suggestions:
            if d.check(suggestion) and not suggestion.isdigit() and not re.findall("[0-9]*[.!?\\-]", suggestion) \
                    and len(suggestion) > 1 and suggestion.lower() not in cand_list:
                compound.append(suggestion)
        cand_list.append(' '.join(compound))


def lookup_dict():
    dict = {}
    with open('../lexicons/all.txt') as f:
        for line in f:
            line = line.rstrip()
            line = line.split('->')
            #  Make dictionary
            k = line[0]
            v = line[1]
            dict[k] = v
    return dict


def lookup_candidates(word, cand_list):
    if word.lower() in lookup_dict:
        suggestion = lookup_dict.get(word.lower())
        cand_list.append(suggestion)


def generate_candidates(word):
    brown_list = []
    distance_list = []
    embeddings_list = []
    enchant_list = []
    w2v_embeddings_list = []
    lookup_list = []

    brown_candidates(word, brown_list)
    distance_candidates(word, distance_list)
    embeddings_candidates(word, embeddings_list)
    w2v_embeddings_candidates(word, w2v_embeddings_list)
    enchant_candidates(word, enchant_list)
    lookup_candidates(word, lookup_list)

    suggestion_list = brown_list + distance_list + embeddings_list + w2v_embeddings_list + enchant_list + lookup_list

    # Retrieve 'best option'
    r = [x for x, count in Counter(suggestion_list).most_common(1) if count >= 2]

    if len(r) >= 1 and len(lookup_list) == 0:
        return r[0]
    elif len(lookup_list) == 1:
        return lookup_list[0]
    else:
        return False


count = 0

lookup_dict = lookup_dict()


with open('../noisy/generated/noisy-10000.txt') as f:
    with open('../noisy/training_data/noisy-10000.txt', 'w') as fw:
        for line in f:
            count += 1
            if 'NEED_NORM' in line:
                noisy_word = line.split()[0]
                if re.findall("[0-9](am|pm|AM|PM)", noisy_word) or re.findall(".(mph)", noisy_word) \
                        or re.findall(".(cm|mm|dm)", noisy_word) or re.findall("[0-9](c|C|F)", noisy_word) \
                        or noisy_word in abbr_list:
                    write_normed(noisy_word)
                elif re.findall(r'((\w)\2{2,}$)', noisy_word):
                    matches = re.findall(r'((\w)\2{2,})', noisy_word)
                    if len(matches) == 1:
                        one_character_rep_word = noisy_word.replace(matches[0][0], matches[0][1])
                        two_character_rep_word = noisy_word.replace(matches[0][0], matches[0][1] * 2)
                        if d.check(two_character_rep_word):
                            write_need_norm(noisy_word, two_character_rep_word)
                        elif d.check(one_character_rep_word) or d.check(one_character_rep_word.lower()):
                            write_need_norm(noisy_word, one_character_rep_word)
                        else:
                            if generate_candidates(noisy_word):
                                clean_word = generate_candidates(noisy_word)
                                write_need_norm(noisy_word, clean_word)
                            else:
                                write_normed(noisy_word)
                elif re.findall(r'((\w)\2{2,})', noisy_word):
                    matches = re.findall(r'((\w)\2{2,})', noisy_word)
                    repetition = [x[0] for x in matches]
                    if len(repetition) == 1:
                        nr_char = len(' '.join(repetition))
                        start_index = noisy_word.index(' '.join(repetition))
                        end_index = start_index + nr_char - 1
                        delete_index_two_char = start_index + 1
                        # First check if 2 characters are enough to make up a good word, then check 1
                        two_character_rep_word = noisy_word[:start_index] + noisy_word[start_index:delete_index_two_char] + noisy_word[end_index:]
                        one_character_rep_word = noisy_word[:start_index] + noisy_word[end_index:]
                        if d.check(two_character_rep_word):
                            write_need_norm(noisy_word, two_character_rep_word)
                        elif d.check(one_character_rep_word):
                            write_need_norm(noisy_word, one_character_rep_word)
                        else:
                            if generate_candidates(noisy_word):
                                clean_word = generate_candidates(noisy_word)
                                write_need_norm(noisy_word, clean_word)
                            else:
                                write_normed(noisy_word)
                elif generate_candidates(noisy_word):
                    clean_word = generate_candidates(noisy_word)
                    write_need_norm(noisy_word, clean_word)
                else:
                    write_normed(noisy_word)
            else:
                fw.write("{}".format(line))











