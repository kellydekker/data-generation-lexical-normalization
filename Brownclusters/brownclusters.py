import random
import string
import re
from difflib import SequenceMatcher
from nltk.tokenize import TweetTokenizer


tokenizer = TweetTokenizer()


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


d = {}


def getKeysByValue(dictOfElements, valueToFind):
    """Get a list of keys from dictionary which has the given value"""
    listOfKeys = list()
    listOfItems = dictOfElements.items()
    for item in listOfItems:
        if item[1] == valueToFind:
            listOfKeys.append(item[0])
    return listOfKeys


def get_keys(dict, value):
    for k, v in dict.iteritems():
        if v == value:
            yield k


def has_numbers(inputstr):
    return any(char.isdigit() for char in inputstr)


# Make dictionary of Twitter Brown clusters from textfile
textfile = '50mpaths.txt'
with open(textfile) as f:
    for line in f:
        k = line.strip().split('\t')[1]
        v = line.strip().split('\t')[0]
        d[k.strip()] = v.strip()

brown_tweets = []

with open('../clean/clean-10000.txt') as f:
    for line in f:
        tweet = []
        words = tokenizer.tokenize(line)
        for word in words:
            #  if word in brown dictionary, put brown cluster in value
            if word in d.keys():
                value = d.get(word)
                tweet.append((word, value))
            else:
                tweet.append(word)
        brown_tweets.append(tweet)

clean_tweets = []
noisy_tweets = []

punc_list = ['_', '-', '#', '/', '\\', '`', '\'', '@']
count = 0


with open('brown-10000.txt', 'w') as f:
    for line in brown_tweets:
        clean_tweet = []
        noisy_tweet = []
        nr_to_replace_words = 0
        max_brown_words = 0
        count += 1

        for word in line:
            options = []
            if isinstance(word, tuple):
                clean_tweet.append(word[0])
                for key, value in d.items():
                    token = word[0]
                    if len(key) > 1:
                        if word[1] == value and not key.isdigit() \
                                and (similar(token, key) >= 0.5 or key[-1].isdigit()) \
                                and not re.findall("[0-9]*[!\"#$%&()*+,-./:;<=>?@[\]^_`{|}~]", key) \
                                and (token[0] == key[0] or token[0].isdigit()) \
                                and (token[-1] == key[-1] or key[-1].isdigit()):
                            options.append(key)
                #  Only replace words that have multiple other 'Brown values'
                if len(options) > 0:
                    brown_word = random.choice(options)  # Select random other word from same Brown cluster
                    number = random.randint(0, 1)  # Random value to select if word will be replaced or not
                    if number == 1 and max_brown_words < 3:  # Only a max of 3 words will be replaced each sentence
                        max_brown_words += 1
                        noisy_tweet.append(brown_word)
                        nr_to_replace_words += 1
                        if word[0] == brown_word:
                            f.write('{}\tNORMED\t{}\n'.format(brown_word, word[0]))
                        else:
                            f.write('{}\tNEED_NORM\t{}\n'.format(brown_word, word[0]))
                    else:
                        f.write('{}\tNORMED\t{}\n'.format(word[0], word[0]))
                        noisy_tweet.append(word[0])
                else:
                    f.write('{}\tNORMED\t{}\n'.format(word[0], word[0]))
            else:  # If the word has no Brown cluster, don't replace it. Just append it and do nothing with it
                f.write('{}\tNORMED\t{}\n'.format(word, word))
                clean_tweet.append(word)
                noisy_tweet.append(word)
        f.write('\n')