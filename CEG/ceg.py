import re
import sys
import csv
import nltk
import random
import string
import pronouncing
import splitter
import enchant
import fileinput
from typodistance import typoGenerator
import argparse
from argparse import RawTextHelpFormatter
from nltk.tokenize import TweetTokenizer
import numpy as np


def write_normed(token):
    fw.write('{}\tNORMED\t{}\n'.format(token, token))


def write_need_norm(oov, token):
    fw.write('{}\tNEED_NORM\t{}\n'.format(oov, token))


def generate(error_list):
    global fw
    max_tweets = 10000
    total_tweets = 0
    with open(args.input) as f:
        with open(training_file, 'w') as fw:
            for line in f:
                if total_tweets < max_tweets:
                    error_category = np.random.choice(error_list)
                    tweet_list = tokenizer.tokenize(line)
                    if error_category == split:
                        error_category(tweet_list, compounds_list)
                        total_tweets += 1
                    else:
                        error_category(tweet_list)
                        total_tweets += 1
                else:
                    break


def missing_apostrophe(tweet):
    for word in tweet:
        chance = np.random.randint(0, 1)
        if re.findall("'s$", word) and chance == 1 and not word[0].isupper():
            no_apostrophe = word.replace('\'s', 'z')
            write_need_norm(no_apostrophe, word)
        elif '\'' in word:
            no_apostrophe = word.replace('\'', '')
            write_need_norm(no_apostrophe, word)
            # fw.write('{}\tNEED_NORM\t{}\n'.format(no_apostrophe, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def repetition(tweet):
    words = tweet
    rand_word = np.random.choice(words)
    repetition_nr = np.random.randint(1, 3)
    repetition_count = 0
    # Add chance of adding repetition based on np.random.int
    for word in words:
        if word == rand_word and not any(char in rand_word for char in string.punctuation) \
                and rand_word != 'RT' and not any(char.isdigit() for char in rand_word) and rand_word != 'USERNAME':
            chance = np.random.randint(0, 2)
            if chance == 1 and repetition_count < 2:
                index = words.index(rand_word)
                repetition_word = rand_word + rand_word[-1] * repetition_nr
                words[index] = repetition_word
                fw.write('{}\tNEED_NORM\t{}\n'.format(repetition_word, word))
            elif chance == 2 and repetition_count < 2:
                if rand_word.islower():
                    # Get all indices for vowels
                    vowel_indices = [idx for idx, ch in enumerate(rand_word) if ch.lower() in 'aeiou']
                    # Select np.random.vowel from the vowel indices
                    if len(vowel_indices) >= 1:
                        rand_id = np.random.choice(vowel_indices)
                        char = rand_word[rand_id].lower()
                        repetition_word = rand_word[:rand_id] + char * repetition_nr + rand_word[rand_id:]
                        fw.write('{}\tNEED_NORM\t{}\n'.format(repetition_word, word))
                        print(repetition_word)

                    else:
                        fw.write('{}\tNORMED\t{}\n'.format(word, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def count_vowels(string, vowels):
    final = [each for each in string if each in vowels]
    return len(final)


def shortening_vowels(tweet):
    words = tweet
    vowel_list = ['e', 'a', 'i', 'o', 'u']
    rand_word = np.random.choice(words)
    index = words.index(rand_word)
    index_nr = -1
    # nr_vowels = count_vowels((rand_word, vowel_list))
    # short_nr = np.random.randint(0, nr_vowels)
    for word in words:
        string = ''
        index_nr += 1
        vowel_count = 0
        if index_nr == index and any(char in vowel_list for char in rand_word) and rand_word != '-user-' and not rand_word.startswith('#'):
            if len(rand_word) >= 3:
                for char in rand_word:
                    if char in 'aeiou':
                        vowel_chance = np.random.randint(0, 1)
                        if vowel_chance == 1:
                            string = string + char
                        else:
                            vowel_count += 1
                    else:
                        string = string + char
                if vowel_count >= 1 and len(string) != 1:
                    fw.write('{}\tNEED_NORM\t{}\n'.format(string, rand_word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def regular(tweet):
    for word in tweet:
        if word.endswith('ing'):
            pos_tag = nltk.pos_tag([word])
            if pos_tag[0][1] == 'VBG' or pos_tag[0][1] == 'VBN':
                chance = random.randint(0, 1)
                if chance == 1 and word.endswith('ting'):
                    phonetic_word = word.replace('ing', 'n')
                    fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
                else:
                    phonetic_word = word.replace('ing', 'in')
                    fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif word.endswith('er') and not word[0].isupper() and len(word) > 5:
                phonetic_word = word.replace('er', 'a')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif word.endswith('ers') and not word[0].isupper() and len(word) > 5:
                phonetic_word = word.replace('er', 'as')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def phonetic(tweet):
    count_z = 0
    for word in tweet:
        if word.endswith('ed') and len(word) > 6:
            phonetic_word = word.replace('ed', 'd')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word.endswith('les'):
            search = re.compile(r'.*[aeiou]les$')  # Search for words that end with les without a vowel before les
            if not search.findall(word):  # e.g. vehicles
                phonetic_word = word.replace('les', 'els')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word.endswith('s') and word[-2] != '\'' and word.count('s') == 1 and count_z < 1 and random.randint(0, 1):
            phonetic_word = word.replace('s', 'z')
            count_z += 1
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word == 'at':
            phonetic_word = word.replace('at', '@')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word == 'your':
            phonetic_word = 'ur'
            write_need_norm(phonetic_word, word)
        elif word == 'you':  # Make common mistake of you -> u
            chance = np.random.randint(0, 2)
            if chance == 0:
                phonetic_word = word.replace('you', 'u')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif chance == 1:
                phonetic_word = word.replace('you', 'ya')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif 'ei' in word:
            search = re.compile(r'.*[aeiou]ei.*[a-z]')
            if search:
                phonetic_word = word.replace('ei', 'ie')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word.endswith('ough'):
            pronouncing_list = pronouncing.phones_for_word(word)[0].split()
            if pronouncing_list[-2:] == ['AH1', 'F']:
                phonetic_word = word.replace('ough', 'uf')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word.endswith('ff'):
            phonetic_word = word.replace('ff', 'f')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word == 'and':
            chance = np.random.randint(0, 2)
            if chance == 1:
                phonetic_word = word.replace('and', 'n')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif chance == 2:
                phonetic_word = word.replace('and', 'nd')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'to' or word == 'too':
            chance = np.random.randint(0, 2)
            if chance == 1 and word == 'to':
                phonetic_word = word.replace('to', '2')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif chance == 2 and word == 'too':
                phonetic_word = word.replace('too', '2')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'thanks':
            phonetic_word = word.replace('thanks', 'thx')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word == 'are':
            chance = np.random.randint(0, 1)
            if chance == 1:
                phonetic_word = word.replace('are', 'r')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif 'to' in word:
            try:
                pronouncing_list = pronouncing.phones_for_word(word)[0].split()
                if pronouncing_list[-2:-1] == ['T', 'AH0']:
                    phonetic_word = word.replace('to', '2')
                    fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            except IndexError as e:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'for':
            chance = np.random.randint(0,1)
            if chance == 1:
                phonetic_word = word.replace('for', '4')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif 'fore' in word:
            phonetic_word = word.replace('fore', '4')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif 'ate' in word:
            try:
                pronouncing_list = pronouncing.phones_for_word(word)[0].split()
                if all(i in pronouncing_list for i in ['EY1', 'T']):
                    phonetic_word = word.replace('ate', '8')
                    fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            except IndexError as e:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'see':
            chance = np.random.randint(0, 1)
            if chance == 1:
                phonetic_word = word.replace('see', 'c')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'why':
            phonetic_word = word.replace('why', 'y')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif 'one' in word:
            try:
                pronouncing_list = pronouncing.phones_for_word(word)[0].split()
                if pronouncing_list[-2:] == ['AH2', 'N']:
                    phonetic_word = word.replace('one', '1')
                    fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            except IndexError as e:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'be':
            if random.randint(0, 1) == 1:
                phonetic_word = word[0]
                write_need_norm(phonetic_word, word)
        elif word == 'because':
            chance = np.random.randint(0, 1)
            if chance == 0:
                phonetic_word = word.replace('because', 'cause')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif chance == 1:
                phonetic_word = word.replace('because', 'bc')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word == 'please':
            chance = np.random.randint(0, 1)
            if chance == 1:
                phonetic_word = word.replace('please', 'pls')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            elif chance == 2:
                phonetic_word = word.replace('please', 'plz')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif re.findall('[^aeiou]y$', word):
            chance = np.random.randint(0,1)
            if chance == 1 and not word[0].isupper() and len(word) > 3:
                phonetic_word = word.replace(word[-1], 'i')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif word.endswith('ight'):
            phonetic_word = word.replace('ight', 'ite')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif 'ch' in word:
            try:
                index = word.index('ch')
                pronouncing_list = pronouncing.phones_for_word(word)[0].split()
                if 'K' in pronouncing_list and not 'CH' in pronouncing_list:
                    phonetic_word = word.replace('ch', 'k')
                    fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            except IndexError as e:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        elif 'ome' in word:
            phonetic_word = word.replace('ome', 'um')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif re.findall('[^u]gh$', word):
            phonetic_word = word.replace('ough', 'o')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word.startswith('ph'):
            phonetic_word = word.replace('ph', 'f')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word.endswith('ould'):
            phonetic_word = word.replace('oul', 'u')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif 'oo' in word:
            phonetic_word = word.replace('oo', 'u')
            fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
        elif word.endswith('ll') and not '\'' in word:
            chance = random.randint(0, 1)
            if chance == 1:
                phonetic_word = word.replace('ll', 'l')
                fw.write('{}\tNEED_NORM\t{}\n'.format(phonetic_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))

        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def spelling(tweet):
    d = {}
    h = {}
    max_mistakes = np.random.randint(0, 3)
    number = np.random.randint(0, 2)
    count = 0

    with open('../wiki/spelling.txt') as f:
        for line in f:
            line = line.rstrip()
            line = line.split('->')
            #  Make dictionary
            k = line[1]
            v = line[0]
            d[k] = v

    with open('homophones') as f:
        for line in f:
            line = line.rstrip()
            line = line.split('->')
            #  Make dictionary
            k = line[1]
            v = line[0]
            h[k] = v

    for word in tweet:
        if word in d.keys() and number == 2 and count < max_mistakes:
            spelling_word = d.get(word)
            count += 1
            fw.write('{}\tNEED_NORM\t{}\n'.format(spelling_word, word))
        elif word in h.keys():
            spelling_word = h.get(word)
            count += 1
            fw.write('{}\tNEED_NORM\t{}\n'.format(spelling_word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def typo(tweet):
    typo_count = 0
    for word in tweet:
        typo_list = []
        if word != '-user-' and word != 'RT' and word != string.punctuation and len(word) > 5 and not word[0].isupper() \
                and not word.startswith('#'):
            # Generate list of typos with (keyboard) edit distance of 2
            for typo in typoGenerator(word, 2):
                if (len(typo) == len(word) or len(typo) == len(word) + 1) and not any(punc in string.punctuation for punc in typo) \
                        and not any(char.isdigit() for char in typo) and not ' ' in typo:
                    typo_list.append(typo)
            if len(typo_list) > 1:
                typo = np.random.choice(typo_list)
                if word != typo and typo_count == 0:
                    typo_count += 1
                    fw.write('{}\tNEED_NORM\t{}\n'.format(typo, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def split(tweet, compounds):
    for word in tweet:
        if word in compounds_list:
            first_word = ''
            index = 0
            max_option = 1
            for letter in word:
                first_word += letter
                index += 1
                second_word = word[index:len(word)]
            if len(first_word) > 1 and len(second_word) and d.check(first_word) and d.check(second_word) \
                    and max_option == 1:
                max_option += 1
                # print(first_word, word, second_word)
                fw.write('{0}\tNEED_NORM\t{1}\n{2}\tNEED_NORM\n'.format(first_word, word, second_word))
            else:
                fw.write('{0}\tNORMED\t{1}\n'.format(word, word))
        else:
            fw.write('{0}\tNORMED\t{1}\n'.format(word, word))
    fw.write('\n')


def shortening(tweet):
    day_list = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    months_list = ['january', 'february', 'march', 'april', 'june', 'july',
              'august', 'september', 'october', 'november', 'december']
    for word in tweet:
        if word.lower() in day_list:
            if word.lower() == 'thursday':
                short_word = 'thur'
                fw.write('{}\tNEED_NORM\t{}\n'.format(short_word, word))
            else:
                short_word = word[:3]
                fw.write('{}\tNEED_NORM\t{}\n'.format(short_word, word))
        elif word.lower() in months_list:
           short_word = word[:3]
           fw.write('{}\tNEED_NORM\t{}\n'.format(short_word, word))
        elif word in shortening_dict.keys():
            short_word = shortening_dict.get(word)
            fw.write('{}\tNEED_NORM\t{}\n'.format(short_word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def slang(tweet):
    for word in tweet:
        if word in slang_dict.keys() and word not in ['you', 'the', 'and', 'You', 'The']:
            spelling_word_list = slang_dict.get(word)
            if len(spelling_word_list) > 1:
                slang_word = np.random.choice(spelling_word_list)

                fw.write('{0}\tNEED_NORM\t{1}\n'.format(slang_word, word))
            elif len(spelling_word_list) == 1:
                slang_word = spelling_word_list[0]
                fw.write('{0}\tNEED_NORM\t{1}\n'.format(slang_word, word))
            else:
                fw.write('{}\tNORMED\t{}\n'.format(word, word))
        else:
            fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def do_nothing(tweet):
    for word in tweet:
        fw.write('{}\tNORMED\t{}\n'.format(word, word))
    fw.write('\n')


def shortening_dict():
    dict = {}
    with open('../lexicons/shortening.txt') as f:
        for line in f:
            line = line.rstrip()
            line = line.split('->')
            #  Make dictionary
            k = line[1]
            v = line[0]
            dict[k] = v
    return dict


if __name__ == '__main__':
    np.random.seed(1337)
    random.seed(1337)

    d = enchant.Dict("en")
    slang_dict = {}
    tokenizer = TweetTokenizer()

    # Make compounds list
    compounds_list = []
    with open('compounds.txt') as f:
        for line in f:
            line = line.rstrip()
            compounds_list.append(line)

    with open('../lexicons/all.txt') as f:
        for line in f:
            line = line.rstrip()
            line = line.split('->')
            #  Make dictionary
            k = line[1].lower()
            v = line[0]

            if k in slang_dict:
                slang_dict[k].append(v)
            else:
                slang_dict[k] = [v]


    shortening_dict = shortening_dict()

    parser = argparse.ArgumentParser(description='Select error categories to generate', formatter_class=RawTextHelpFormatter)

    parser.add_argument('-l', '--list', nargs='+', help='Enter the category you want to exclude generate errors. '
                                                        'All available options are:'
                                                        '\nmissing_apostrophe'
                                                        '\nrepetition\n'
                                                        'shortening_vowels\n'
                                                        'phonetic\n'
                                                        'typo\n'
                                                        'split\n'
                                                        'shortening\n'
                                                        'spelling\n'
                                                        'slang\n')
    parser.add_argument('-a', '--all', help='Selects all error categories')
    parser.add_argument('-m', '--more', help='Uses all categories and increases the chance of more errors of selected category')
    parser.add_argument('-d', '--delete', help='Uses all categories excluding the category selected here')

    parser.add_argument('-i', '--input', help='File with full content of tweet each line')
    parser.add_argument('-o', '--output', help='Filename of the generated training set')

    args = parser.parse_args()

    training_file = args.output

    if args.all:
        error_categories = [missing_apostrophe, repetition, shortening_vowels, phonetic, typo, split, shortening,
                            spelling, slang, regular]
        print('Generating non-standard words...')
        generate(error_categories)

    elif args.more:
        error_categories = [missing_apostrophe, repetition, shortening_vowels, phonetic, typo, split, shortening,
                            spelling, slang, regular, eval(args.more)]
        print('Generating non-standard words...')
        generate(error_categories)
    elif args.delete:
        error_categories = [missing_apostrophe, repetition, shortening_vowels, phonetic, typo, split, shortening,
                            spelling, slang, regular]
        error_categories.remove(eval(args.delete))
        print(error_categories)
        print('Generating non-standard words...')
        generate(error_categories)
    elif args.list:
        for _, value in args._get_kwargs():
            if value is not None:
                generate([value])
    else:
        print('No arguments selected')

