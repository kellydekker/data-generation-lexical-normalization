from embeddings import Embeds
import string
import nltk
import random
import emoji
import re
from nltk.metrics import edit_distance
from nltk.tokenize import TweetTokenizer


def extract_emojis(str):
    return ''.join(c for c in str if c in emoji.UNICODE_EMOJI)


# Load MoNoise embeddings model
embeddings = Embeds()
embeddings.loadBin('en.tw.embeds.bin')
tokenizer = TweetTokenizer()


random.seed(1)


with open('smwe/smwe-3000.txt', 'w') as fw:
    with open('../clean/clean-3000.txt') as f:
        for line in f:
            words = tokenizer.tokenize(line)
            for word in words:
                if embeddings.find(word) and word not in string.punctuation and not (word.isdigit() or word[0].isdigit()):
                    suggestions_list = []
                    suggestions = embeddings.find(word)
                    chance = random.randint(0, 4)  # Add chance to decrease number of words replaced
                    for suggestion in suggestions:
                        if len(suggestion) > 1 and len(word) > 1 \
                                and (suggestion[0] == word[0] or re.findall("^[0-9]([A-Z]|[a-z])", suggestion)) \
                                and (suggestion[-1] == word[-1] or suggestion[-1] == word[-2] or suggestion[-1].isdigit()) \
                                and suggestion.lower() != word.lower()\
                                and not extract_emojis(suggestion) \
                                and edit_distance(word, suggestion) <= 4 \
                                and len(re.findall("[,@\'?\.$%_…/;!:\"]", suggestion)) < 1:
                            suggestions_list.append(suggestion)
                    if len(suggestions_list) >= 1 and chance == 1:
                        noisy_word = random.choice(suggestions_list)
                        fw.write('{}\tNEED_NORM\t{}\n'.format(noisy_word, word))
                    else:
                        fw.write('{}\tNORMED\t{}\n'.format(word, word))
                else:
                    fw.write('{}\tNORMED\t{}\n'.format(word, word))
            fw.write('\n')

