import re
import random
import emoji
import enchant
import numpy as np
from nltk.metrics import edit_distance
from nltk.tokenize import TweetTokenizer
from gensim.models.keyedvectors import KeyedVectors

seed = 1
np.random.seed(seed)

glove_data_file = 'glove/glove.twitter.27B.50d.txt'
d = enchant.Dict("en")
tokenizer = TweetTokenizer()
model = KeyedVectors.load_word2vec_format("gensim_glove_vectors.txt", binary=False)
print("embedding model loaded")


# Function to determine if a generated candidate 'makes sense'
def is_ok(term, candidates):
    suggestions_list = []
    for suggestion in candidates:
        suggestion = suggestion[0]
        if (len(suggestion) > 1 and len(term) > 1) \
                and (suggestion[0] == term[0] or suggestion[0].isdigit()) \
                and (suggestion[-1] == term[-1] or suggestion[-1] == term[-2] or suggestion[-1].isdigit()) \
                and suggestion != term \
                and not extract_emojis(suggestion) \
                and len(re.findall("[,@\'?\.$%_…/#]", suggestion)) < 1 \
                and edit_distance(term, suggestion) <= 5 \
                and not (d.check(suggestion) or d.check(suggestion.capitalize())):
            suggestions_list.append(suggestion)
    if len(suggestions_list) >= 1:
        noisy_term = random.choice(suggestions_list)
        return noisy_term, term


def extract_emojis(str):
    return ''.join(c for c in str if c in emoji.UNICODE_EMOJI)


with open('glove_data/glove-2000.txt', 'w') as fw:
    with open('../clean/clean-2000.txt') as f:
        for line in f:
            words = tokenizer.tokenize(line)
            for word in words:
                try:
                    suggestions = model.most_similar(word, topn=50)
                    if is_ok(word, suggestions):
                        noisy_word, word = is_ok(word, suggestions)
                        print(noisy_word, word)
                        fw.write('{}\tNEED_NORM\t{}\n'.format(noisy_word, word))
                    else:
                        fw.write('{}\tNORMED\t{}\n'.format(word, word))
                except KeyError:
                    try:
                        # Check for suggestions if word is lowercased
                        word = word.lower()
                        suggestions = model.most_similar(word, topn=50)
                        if is_ok(word, suggestions) and word != 'username':
                            noisy_word, word = is_ok(word, suggestions)
                            fw.write('{}\tNEED_NORM\t{}\n'.format(noisy_word, word))
                        else:
                            fw.write('{}\tNORMED\t{}\n'.format(word, word))
                    except KeyError:
                        fw.write('{}\tNORMED\t{}\n'.format(word, word))
            fw.write('\n')  # Write newline after every tweet
